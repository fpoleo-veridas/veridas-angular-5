This is the list of document types and country codes that may be used with the _documents_ configuration setting. For TD1 size documents.  
It is possible to configure the GROUP_ID for select multiple documents, or the document ID for unique document.

**Examples**  
documents: 'ES2'  
documents: 'ES2_ID'  
documents: 'ES_DrivingLicense_2013'

## TWO SIDED DOCUMENTS

### Andorra

- AD_DrivingLicense_1990 (GROUP_ID: AD, AD_DL)

### Albania

- AL_IDCard_2009 (GROUP_ID: AL, AL_ID)

### Argentina

- AR_IDCard_2012 (GROUP_ID: AR2, AR2_ID)
- AR_IDCard_2009 (GROUP_ID: AR2, AR2_ID)
- ARG2009 (GROUP_ID: AR, AR_ID)
- ARG2012 (GROUP_ID: AR, AR_ID)

### Australia

**Western**

- AU-WA_DrivingLicense_2011 (GROUP_ID: AU-WA, AU-WA_DL)
- AU-WA_DrivingLicense_2014 (GROUP_ID: AU-WA, AU-WA_DL)

**Tasmania**

- AU-TAS_DrivingLicense_2015 (GROUP_ID: AU-TAS, AU-TAS_DL)

**Queensland**

- AU-QLD_DrivingLicense_2011 (GROUP_ID: AU-QLD, AU-QLD_DL)
- AU-QLD_DrivingLicense_2016 (GROUP_ID: AU-QLD, AU-QLD_DL)

**Northern Territory**

- AU-NT_DrivingLicense_2006 (GROUP_ID: AU-NT, AU-NT_DL)

**New South Wales**

- AU-NSW_DrivingLicense_2013 (GROUP_ID: AU-NSW, AU-NSW_DL)

**South Australia**

- AU-SA_DrivingLicense_2014 (GROUP_ID: AU-SA, AU-SA_DL)

**Victoria**

- AU-VIC_DrivingLicense_2009 (GROUP_ID: AU-VIC, AU-VIC_DL)

**Australian Capital Territory**

- AU-ACT_DrivingLicense_2011 (GROUP_ID: AU-ACT, AU-ACT_DL)

### Austria

- AT_DrivingLicense_2006 (GROUP_ID: AT, AT_DL)
- AT_DrivingLicense_2014 (GROUP_ID: AT, AT_DL)
- AT_IDCard_2010 (GROUP_ID: AT, AT_ID)
- AT_IDCard_2002 (GROUP_ID: AT, AT_ID)
- AT_DrivingLicense_2004
- AT_ResidencePermit_2011 (GROUP_ID: AT, AT_ID)
- AT_ResidencePermit_2005 (GROUP_ID: AT, AT_ID)

### Bosnia

- BA_IDCard_2003 (GROUP_ID: BA, BA_ID)
- BA_IDCard_2013 (GROUP_ID: BA, BA_ID)

### Belgium

- BE_DrivingLicense_2010 (GROUP_ID: BE, BE_DL)
- BE_DrivingLicense_2013 (GROUP_ID: BE, BE_DL)
- BE_IDCard_2008 (GROUP_ID: BE, BE_ID)
- BE_IDCard_2010 (GROUP_ID: BE, BE_ID)

### Brasil

- BR_DrivingLicense_2017 (GROUP_ID: BR, BR_DL)
  This document needs the following setup\* configuration:

```javascript
const VDDocument = makeVDDocumentWidget();
VDDocument({
  targetSelector: '#target',
  infoAlertShow: true,
  reviewImage: true,
  BRDL: ['BR_DrivingLicense_2017'],
  displayErrors: true,
  displayDetectionCanvas: false,
  displayCapabilitiesDebugger: false,
  logEventsToConsole: true,
  displayIP: false,
  recordMetrics: true,
});
```

- BR_IDCard_2014 (GROUP_ID: BR, BR_ID)
- BR_DrivingLicense_2019 (GROUP_ID: BR, BR_DL)

### Bulgaria

- BG_DrivingLicense_2002 (GROUP_ID: BG, BG_DL)
- BG_DrivingLicense_2013 (GROUP_ID: BG, BG_DL)
- BG_IDCard_2006 (GROUP_ID: BG, BG_ID)
- BG_IDCard_2010 (GROUP_ID: BG, BG_ID)

### Belarus

- BY_DrivingLicense_2010 (GROUP_ID: BY, BY_DL)

### Switzerland

- CH_DrivingLicense_2003 (GROUP_ID: CH, CH_DL)
- CH_IDCard_2003 (GROUP_ID: CH, CH_ID)

### Chile

- CL_IDCard_2002 (GROUP_ID: CL, CL_ID)
- CL_IDCard_2013 (GROUP_ID: CL, CL_ID)

### China

- CN_IDCard_2004 (GROUP_ID: CN, CN_ID)

### Colombia

- CO_IDCard_2000 (GROUP_ID: CO, CO_ID)
- CO_ResidencePermit_2016 (GROUP_ID: CO, CO_ID)

### Croatia

- HR_DrivingLicense_2013 (GROUP_ID: HR, HR_DL)
- HR_IDCard_2003 (GROUP_ID: HR, HR_ID)
- HR_IDCard_2015 (GROUP_ID: HR, HR_ID)

### Cyprus

- CY_DrivingLicense_2015 (GROUP_ID: CY, CY_DL)
- CY_IDCard_2008 (GROUP_ID: CY, CY_ID)
- CY_IDCard_2015 (GROUP_ID: CY, CY_ID)

### Czechia

- CZ_DrivingLicense_2013 (GROUP_ID: CZ, CZ_DL)
- CZ_IDCard_2003 (GROUP_ID: CZ, CZ_ID)
- CZ_IDCard_2014 (GROUP_ID: CZ, CZ_ID)

### Germany

- DE_DrivingLicense_2004 (GROUP_ID: DE, DE_DL)
- DE_DrivingLicense_2013 (GROUP_ID: DE, DE_DL)
- DE_IDCard_2007 (GROUP_ID: DE, DE_ID)
- DE_IDCard_2010 (GROUP_ID: DE, DE_ID)

### Denmark

- DK_DrivingLicense_1997 (GROUP_ID: DK, DK_DL)
- DK_DrivingLicense_2013 (GROUP_ID: DK, DK_DL)

### Estonia

- EE_DrivingLicense_2004 (GROUP_ID: EE, EE_DL)
- EE_DrivingLicense_2013 (GROUP_ID: EE, EE_DL)
- EE_IDCard_2011 (GROUP_ID: EE, EE_ID)

### Finland

- FI_DrivingLicense_1992 (GROUP_ID: FI, FI_DL)
- FI_DrivingLicense_2010 (GROUP_ID: FI, FI_DL)
- FI_DrivingLicense_2013 (GROUP_ID: FI, FI_DL)
- FI_IDCard_2011 (GROUP_ID: FI, FI_ID)
- FI_IDCard_2017 (GROUP_ID: FI, FI_ID)

### France

- FR_DrivingLicense_2013 (GROUP_ID: FR, FR_DL)
- FR_IDCard_1994 (GROUP_ID: FR, FR_ID)

### Great Britain

- GB_DrivingLicense_1998 (GROUP_ID: GB, GB_DL)
- GB_DrivingLicense_2007 (GROUP_ID: GB, GB_DL)
- GB_DrivingLicense_2014 (GROUP_ID: GB, GB_DL)
- GB_DrivingLicense_2015 (GROUP_ID: GB, GB_DL)
- GB_DrivingLicense-PL_1998 (GROUP_ID: GB, GB_DL)
- GB_DrivingLicense-PL_2007 (GROUP_ID: GB, GB_DL)
- GB_DrivingLicense-PL_2014 (GROUP_ID: GB, GB_DL)
- GB_DrivingLicense-PL_2015 (GROUP_ID: GB, GB_DL)

### Greece

- GR_DrivingLicense_2013 (GROUP_ID: GR, GR_DL)

### Guatemala

- GT_IDCard_2009 (GROUP_ID: GT, GT_ID)

### Hungary

- HU_DrivingLicense_2013 (GROUP_ID: HU, HU_DL)
- HU_IDCard_2000 (GROUP_ID: HU, HU_ID)
- HU_IDCard_2015 (GROUP_ID: HU, HU_ID)

### Ireland

- IE_DrivingLicense_2013 (GROUP_ID: IE, IE_DL)
- IE_Passport_2015 (GROUP_ID: IE)

### Mexico

- MX_IDCard_2013
- MX_IDCard_2014 (GROUP_ID: MX2, MX2_ID)
- MX_IDCard_2008 (GROUP_ID: MX2, MX2_ID)
- MX_IDCard_2019 (GROUP_ID: MX2, MX2_ID)
- IFE2002
- IFE2008 (GROUP_ID: MX, MX_ID)
- IFE2013
- IFEE (GROUP_ID: MX, MX_ID)

### Iceland

- IS_DrivingLicense_2001 (GROUP_ID: IS, IS_DL)
- IS_DrivingLicense_2013 (GROUP_ID: IS, IS_DL)

### Italy

- IT_DrivingLicense_2000 (GROUP_ID: IT, IT_DL)
- IT_DrivingLicense_2013 (GROUP_ID: IT, IT_DL)
- IT_IDCard_2004 (GROUP_ID: IT, IT_ID)
- IT_IDCard_2016 (GROUP_ID: IT, IT_ID)
- IT_IDCard_2017 (GROUP_ID: IT, IT_ID)

### Liechtenstein

- LI_DrivingLicense_2003 (GROUP_ID: LI, LI_DL)
- LI_IDCard_1995 (GROUP_ID: LI, LI_ID)
- LI_IDCard_2009 (GROUP_ID: LI, LI_ID)

### Lithuania

- LT_DrivingLicense_2007 (GROUP_ID: LT, LT_DL)
- LT_DrivingLicense_2016 (GROUP_ID: LT, LT_DL)
- LT_IDCard_2002 (GROUP_ID: LT, LT_ID)
- LT_IDCard_2009 (GROUP_ID: LT, LT_ID)

### Luxembourg

- LU_DrivingLicense_2013 (GROUP_ID: LU, LU_DL)
- LU_IDCard_2014 (GROUP_ID: LU, LU_ID)

### Latvia

- LV_DrivingLicense_2004 (GROUP_ID: LV, LV_DL)
- LV_DrivingLicense_2013 (GROUP_ID: LV, LV_DL)
- LV_IDCard_2012 (GROUP_ID: LV, LV_ID)

### Monaco

- MC_IDCard_2009 (GROUP_ID: MC, MC_ID)

### Moldavia

- MD_IDCard_2015 (GROUP_ID: MD, MD_ID)

### Montenegro

- ME_IDCard_2008 (GROUP_ID: ME, ME_ID)

### Macedonia

- MK_IDCard_2007 (GROUP_ID: MK, MK_ID)

### Malta

- MT_DrivingLicense_2003 (GROUP_ID: MT, MT_DL)
- MT_DrivingLicense_2013 (GROUP_ID: MT, MT_DL)
- MT_IDCard_2002 (GROUP_ID: MT, MT_ID)
- MT_IDCard_2014 (GROUP_ID: MT, MT_ID)

### Malasya

- MYS2001 (GROUP_ID: MY, MY_ID)

### Netherlands

- NL_DrivingLicense_2006 (GROUP_ID: NL, NL_DL)
- NL_DrivingLicense_2013 (GROUP_ID: NL, NL_DL)
- NL_DrivingLicense_2014 (GROUP_ID: NL, NL_DL)
- NL_IDCard_2011 (GROUP_ID: NL, NL_ID)
- NL_IDCard_2014 (GROUP_ID: NL, NL_ID)

### Norway

- NO_DrivingLicense_1998 (GROUP_ID: NO, NO_DL)
- NO_DrivingLicense_2007 (GROUP_ID: NO, NO_DL)
- NO_DrivingLicense_2013 (GROUP_ID: NO, NO_DL)

### New Zealand

- NZ_DrivingLicense_2007 (GROUP_ID: NZ, NZ_DL)

### Peru

- PE_IDCard_2007 (GROUP_ID: PE, PE_ID)
- PE_IDCard_2013 (GROUP_ID: PE, PE_ID)
- PE_IDCard_2020 (GROUP_ID: PE, PE_ID)

### Poland

- PL_DrivingLicense_1999 (GROUP_ID: PL, PL_DL)
- PL_DrivingLicense_2004 (GROUP_ID: PL, PL_DL)
- PL_DrivingLicense_2013 (GROUP_ID: PL, PL_DL)
- PL_IDCard_2001 (GROUP_ID: PL, PL_ID)
- PL_IDCard_2015 (GROUP_ID: PL, PL_ID)
- PL_IDCard_2019 (GROUP_ID: PL, PL_ID)

### Portugal

- PT_DrivingLicense_2013 (GROUP_ID: PT, PT_DL)
- PT_DrivingLicense_1999 (GROUP_ID: PT, PT_DL)
- PT_IDCard_2015 (GROUP_ID: PT, PT_ID)

### Panama

- PA_IDCard_2010 (GROUP_ID: PA, PA_ID)

### Paraguay

- PY_IDCard_2007 (GROUP_ID: PY, PY_ID)
- PY_IDCard_2009 (GROUP_ID: PY, PY_ID)

### Philippines

- PH_DrivingLicense_2017 (GROUP_ID: PH, PH_ID)
- PH_IDCard_2011 (GROUP_ID: PH, PH_ID)
- PH_IDCard_2015 (GROUP_ID: PH, PH_ID)
- PH_IDCard_2016 (GROUP_ID: PH, PH_ID)
- PH_IDCard-PO_2016 (GROUP_ID: PH, PH_ID)

### Russia

- RU_DrivingLicense_2011 (GROUP_ID: RU, RU_DL)

### Serbia

- RS_IDCard_2008 (GROUP_ID: RS, RS_ID)

### Singapour

- SG_IDCard_2011 (GROUP_ID: SG, SG_ID)

### Sweden

- SE_DrivingLicense_2016 (GROUP_ID: SE, SE_DL)
- SE_DrivingLicense_2013 (GROUP_ID: SE, SE_DL)
- SE_IDCard_2012 (GROUP_ID: SE, SE_ID)

### Slovenia

- SI_DrivingLicense_2009 (GROUP_ID: SI, SI_DL)
- SI_DrivingLicense_2013 (GROUP_ID: SI, SI_DL)
- SI_IDCard_1998 (GROUP_ID: SI, SI_ID)

### Slovakia

- SK_DrivingLicense_2008 (GROUP_ID: SK, SK_DL)
- SK_DrivingLicense_2013 (GROUP_ID: SK, SK_DL)
- SK_IDCard_2015 (GROUP_ID: SK, SK_ID)

### Spain

- ES_DrivingLicense_2013 (GROUP_ID: ES2, ES2_DL)
- ES_DrivingLicense_2004 (GROUP_ID: ES2, ES2_DL)
- ES_IDCard_2015 (GROUP_ID: ES2, ES2_ID)
- ES_IDCard_2006 (GROUP_ID: ES2, ES2_ID)
- ES_ResidencePermit_2010 (GROUP_ID: ES2, ES2_ID)
- ES_ResidencePermit_2011 (GROUP_ID: ES2, ES2_ID)
- ES_ResidencePermit_2020 (GROUP_ID: ES2, ES2_ID)
- DNI20 (GROUP_ID: ES, ES_ID)
- DNI30 (GROUP_ID: ES, ES_ID)
- NIE2003 (GROUP_ID: ES, ES_ID)
- NIE2010 (GROUP_ID: ES, ES_ID)
- NIE2011 (GROUP_ID: ES, ES_ID)

### Turkey

- TR_IDCard_2016 (GROUP_ID: TR, TR_ID)

### Ukraine

- UA_IDCard_2016 (GROUP_ID: UA, UA_ID)

### Uruguay

- UY_IDCard_1999 (GROUP_ID: UY, UY_ID)
- UY_IDcard_2015 (GROUP_ID: UY, UY_ID)

### USA

These three documents are special. The group id of them are US-XX where XX is the ID of the state (Example: US-AK)

- US_IDCard-MilitaryRS_1993
- US_IDCard-MilitaryRT_1993
- US_IDCard-MilitaryRR_1993

**Alaska** (GROUP_ID: US-AK, US-AK_DL)

- US-AK_DrivingLicense_2014
- US-AK_DrivingLicense_2005
- US-AK_DrivingLicense_2018

**Alabama** (GROUP_ID: US-AL, US-AL_DL)

- US-Al_DrivingLicense_2013 (GROUP_ID: US-AL, US-AL_DL)
- US-AL_IDCard_2013 (GROUP_ID: US-AL, US-AL_ID)

**Arkansas** (GROUP_ID: US-AR, US-AR_DL)

- US-AR_DrivingLicense_2016
- US-AR_DrivingLicense_2018

**Arizona** (GROUP_ID: US-AZ, US-AZ_DL)

- US-AZ_DrivingLicense_1996
- US-AZ_DrivingLicense_1990
- US-AZ_DrivingLicense_2004
- US-AZ_DrivingLicense_2016

**California** (GROUP_ID: US-CA, US-CA_DL)

- US-CA_DrivingLicense_2008
- US-CA_DrivingLicense_2018

**Colorado** (GROUP_ID: US-CO, US-CO_DL)

- US-CO_DrivingLicense_2016
- US-CO_DrivingLicense_2011

**Connecticut** (GROUP_ID: US-CT, US-CT_DL)

- US-CT_DrivingLicense_2017
- US-CT_DrivingLicense_2009

**District of Columbia** (GROUP_ID: US-DC, US-DC_DL)

- US-DC_DrivingLicense_2017

**Delware** (GROUP_ID: US-DE, US-DE_DL)

- US-DE_DrivingLicense_2010
- US-DE_DrivingLicense_2018

**Florida** (GROUP_ID: US-FL, US-FL_DL)

- US-FL_DrivingLicense_2010
- US-FL_DrivingLicense_2017

**Georgia** (GROUP_ID: US-GA, US-GA_DL)

- US-GA_DrivingLicense_2012
- US-GA_DrivingLicense_2007

**Hawaii** (GROUP_ID: US-HI, US-HI_DL)

- US-HI_DrivingLicense_2012
- US-HI_DrivingLicense_2018

**Iowa** (GROUP_ID: US-IA, US-IA_DL)

- US-IA_DrivingLicense_2013
- US-IA_DrivingLicense_2018

**Idaho** (GROUP_ID: US-ID, US-ID_DL)

- US-ID_DrivingLicense_2004
- US-ID_DrivingLicense_2010
- US-ID_DrivingLicense_2017

**Illinois** (GROUP_ID: US-IL, US-IL_DL)

- US-IL_DrivingLicense_2016
- US-IL_DrivingLicense_2007

**Indiana** (GROUP_ID: US-IN, US-IN_DL)

- US-IN_DrivingLicense_2010
- US-IN_DrivingLicense_2017

**Kansas** (GROUP_ID: US-KS, US-KS_DL)

- US-KS_DrivingLicense_2017
- US-KS_DrivingLicense_2012
- US-KS_DrivingLicense_2004

**Kentucky** (GROUP_ID: US-KY, US-KY_DL)

- US-KY_DrivingLicense_2012
- US-KY_DrivingLicense_2019

**Louisiana** (GROUP_ID: US-LA, US-LA_DL)

- US-LA_DrivingLicense_2011
- US-LA_DrivingLicense_2014
- US-LA_DrivingLicense_2016

**Massachusetts** (GROUP_ID: US-MA, US-MA_DL)

- US-MA_DrivingLicense_2018
- US-MA_DrivingLicense_2010

**Maryland** (GROUP_ID: US-MD, US-MD_DL)

- US-MD_DrivingLicense_2013
- US-MD_DrivingLicense_2016

**Maine** (GROUP_ID: US-ME, US-ME_DL)

- US-ME_DrivingLicense_2011

**Michigan** (GROUP_ID: US-MI, US-MI_DL)

- US-MI_DrivingLicense_2017

**Minnesota** (GROUP_ID: US-MN, US-MN_DL)

- US-MN_DrivingLicense_2018
- US-MN_DrivingLicense_2004
- US-MN_DrivingLicense_2014

**Missouri** (GROUP_ID: US-MO, US-MO_DL)

- US-MO_DrivingLicense_2012
- US-MO_DrivingLicense_2004

**Mississippi** (GROUP_ID: US-MS, US-MS_DL)

- US-MS_DrivingLicense_2017
- US-MS_DrivingLicense_2001

**Montana** (GROUP_ID: US-MT, US-MT_DL)

- US-MT_DrivingLicense_2000
- US-MT_DrivingLicense_2008

**North Carolina** (GROUP_ID: US-NC, US-NC_DL)

- US-NC_DrivingLicense_2017
- US-NC_DrivingLicense_2007

**North Dakota** (GROUP_ID: US-ND, US-ND_DL)

- US-ND_DrivingLicense_2018
- US-ND_DrivingLicense_2006

**Nebraska** (GROUP_ID: US-NE, US-NE_DL)

- US-NE_DrivingLicense_2013

**New Hampshire** (GROUP_ID: US-NH, US-NH_DL)

- US-NH_DrivingLicense_2017
- US-NH_DrivingLicense_2006

**New Jersey** (GROUP_ID: US-NJ, US-NJ_DL)

- US-NJ_DrivingLicense_2011

**New Mexico** (GROUP_ID: US-NM, US-NM_DL)

- US-NM_DrivingLicense_2016
- US-NM_DrivingLicense_2014

**Nevada** (GROUP_ID: US-NV, US-NV_DL)

- US-NV_DrivingLicense_2014

**New York** (GROUP_ID: US-NY, US-NY_DL)

- US-NY_DrivingLicense_2008
- US-NY_DrivingLicense_2017

**Ohio** (GROUP_ID: US-OH, US-OH_DL)

- US-OH_DrivingLicense_2018
- US-OH_DrivingLicense_2014
- US-OH_DrivingLicense_2013

**Oklahoma** (GROUP_ID: US-OK, US-OK_DL)

- US-OK_DrivingLicense_2018

**Oregon** (GROUP_ID: US-OR, US-OR_DL)

- US-OR_DrivingLicense_2007
- US-OR_DrivingLicense_2018

**Pennsylvania** (GROUP_ID: US-PA, US-PA_DL)

- US-PA_DrivingLicense_2017
- US-PA_DrivingLicense_2011

**Rhode Island** (GROUP_ID: US-RI, US-RI_DL)

- US-RI_DrivingLicense_2018
- US-RI_DrivingLicense_2008

**South Carolina** (GROUP_ID: US-SC, US-SC_DL)

- US-SC_DrivingLicense_2018
- US-SC_DrivingLicense_2011

**South Dakota** (GROUP_ID: US-SD, US-SD_DL)

- US-SD_DrivingLicense_2009
- US-SD_DrivingLicense_2010

**Tennessee** (GROUP_ID: US-TN, US-TN_DL)

- US-TN_DrivingLicense_2012
- US-TN_DrivingLicense_2003

**Texas**

- US-TX_DrivingLicense_2016 (GROUP_ID: US-TX, US-TX_DL)
- US-TX_IDCard_2016 (GROUP_ID: US-TX, US-TX_ID)

**Utah** (GROUP_ID: US-UT, US-UT_DL)

- US-UT_DrivingLicense_2016
- US-UT_DrivingLicense_2006

**Virginia**

- US-VA_DrivingLicense_2018 (GROUP_ID: US-VA, US-VA_DL)
- US-VA_IDCard_2018 (GROUP_ID: US-VA, US-VA_ID)

**Vermont** (GROUP_ID: US-VT, US-VT_DL)

- US-VT_DrivingLicense_2014
- US-VT_DrivingLicense_2018

**Washington** (GROUP_ID: US-WA, US-WA_DL)

- US-WA_DrivingLicense_2019
- US-WA_DrivingLicense_2010

**Wisconsin** (GROUP_ID: US-WI, US-WI_DL)

- US-WI_DrivingLicense_2015
- US-WI_DrivingLicense_2012
- US-WI_DrivingLicense_2005

**West Virginia** (GROUP_ID:US-WV, US-WV_DL)

- US-WV_DrivingLicense_2013
- US-WV_DrivingLicense_2005

**Wyoming** (GROUP_ID:US-WY, US-WY_DL)

- US-WY_DrivingLicense_2014

### Canada

**Alberta** (GROUP_ID:CA-AB, CA-AB_DL)

- CA-AB_DrivingLicense_2009

**British Columbia** (GROUP_ID:CA-BC, CA-BC_DL)

- CA-BC_DrivingLicense_2013

**Manitoba** (GROUP_ID:CA-MB, CA-MB_DL)

- CA-MB_DrivingLicense_2014

**Newfoundland and Labrador** (GROUP_ID:CA-NL, CA-NL_DL)

- CA-NL_DrivingLicense_2017

**Northwest Territories** (GROUP_ID:CA-NT, CA-NT_DL)

- CA-NT_DrivingLicense_2005

**Nova Scotia** (GROUP_ID:CA-NS, CA-NS_DL)

- CA-NS_DrivingLicense_2017

**Nunavut** (GROUP_ID:CA-NU, CA-NU_DL)

- CA-NU_DrivingLicense_2009

**Ontario** (GROUP_ID:CA-ON, CA-ON_DL)

- CA-ON_DrivingLicense_2007

**Prince Edward Island** (GROUP_ID:CA-PE, CA-PE_DL)

- CA-PE_DrivingLicense_2017

**Quebec State** (GROUP_ID:CA-QC, CA-QC_DL)

- CA-QC_DrivingLicense_2015

**Saskatchewan State**(GROUP_ID:CA-SK, CA-SK_DL)

- CA-SK_DrivingLicense_2016

**Yukon** (GROUP_ID:CA-YT, CA-YT_DL)

- CA-YT_DrivingLicense_2010

**New Brunswick**

- CA-NB_DrivingLicense_2017 (GROUP_ID: CA-NB, CA-NB_DL)
- CA-NB_IDCard_2020 (GROUP_ID: CA-NB, CA-NB_DL)

### Dominican Republic

- DO_IDCard_1998 (GROUP_ID: DO, DO_ID)
- DO_IDCard_2014 (GROUP_ID: DO, DO_ID)

### Malaysia

- MY_IDCard_2012 (GROUP_ID: MY, MY_ID)

### Venezuela

- VE_IDCard_2011 (GROUP_ID: VE, VE_ID)

## ONE SIDED DOCUMENTS

List of document types that may be used with the documentsMinimal configuration setting. For TD1 size documents.

- AT_IDCard_2010: Austria ID Card 2010
- AT_ResidencePermit_2005: Austria Residence Permit 2005

**Example**:  
documentsMinimal : [‘AT_IDCard_2010’] -> Generic document

## AUTOMATICALLY CLASSIFIED DOCUMENTS

The recognised documents that can be automatically classified with their IDs are:

- ES_IDCard_2006: Spain IDCard 2006 (DNI 2.0)
- ES_IDCard_2015: Spain IDCard 2015 (DNI 3.0)
- ES_ResidencePermit_2010: Spain ResidencePermit 2010 (NIE 2010)
- ES_ResidencePermit_2011: Spain ResidencePermit 2011 (NIE 2011)
- ES_ResidencePermit_2020: Spain ResidencePermit 2020 (NIE 2020)
- MX_IDCard_2008: Mexico IFE 2008 version C.
- MX_IDCard_2014: Mexico IFE 2014 version D, E and F.
- MX_IDCard_2019: Mexico IFE 2019 version G.

If one of these documents is being detecting, it is recomended to use the obverseDetection_type and reverseDetection_type to get the ID of the right document and send it with the image captured to the server.

## PASSPORTS

List of document types that may be used with the passport configuration setting. For TD3 size documents.

- XX_Passport_YYYY

**Example**:
passport : [‘XX_Passport_YYYY’] -> Generic passport

## LARGE FORMAT DRIVERS LICENSES

List of document types that may be used with the documentsPaper configuration setting:

- AT_DrivingLicense_2004 (Austria Driving License 2004)

**Example**:  
documentsPaper : [‘AT_DrivingLicense_2004’] -> Austria Driving License 2004

## OTHER DOCUMENTS

List of document types that may be used with documents, documentsMinimal and passport configuration settings:

- XX_XX_XXXX

**Example**:  
documents: [‘XX_XX_XXXX’] -> Generic document
documentsMinimal : [‘XX_XX_XXXX’] -> Generic document
passport : [‘XX_XX_XXXX’] -> Generic document
