There is a list of the different customizable parameters of the SDK

## SDK Configuration

- **blurDetectorActive** (Boolean) -> Whether to use a blur detector when document image has been capture.

- **detectionTimeout** (Number) -> Duration in milliseconds after which VDSelfie should stop recording and shut down.
  Max is 300000, min 60000.

- **documents** (Array of strings) -> Each unique string represents either a TD1 document type (“ES*DrivingLicense_2013”) or a country (“ES”, all supported TD1 document types for that country). See Appendix-A for the list of supported document types and country codes. This setting can not be used in conjunction with \_documentsMinimal*, _passport_ and _documentsPaper_. **Examples**: documents: [‘ES2’] -> All Spain supported documents; documents: [‘ES2_ID’] -> Spain Identity Documents; documents: [‘ES_DrivingLicense_2004’]

- **documentsMinimal** (Array of strings) -> Works the same way the _documents_ configuration setting does, except that it will capture the obverse only. Useful for documents that contain information on one physical side only, or for two-sided documents that support single-side processing. This setting can not be used in conjunction with _documents_, _passport_ and _documentsPaper_. **Example**: documentsMinimal: [‘AT_IDCard_2010’]

- **documentsPaper** (Array of strings) -> Works the same way the _documents_ configuration setting does, and is used for large-format drivers’ licenses. This setting can not be used in conjunction with _documents_, _documentsMinimal_ and _passport_. **Example**: documentsPaper: [‘AT_DrivingLicense_2004’]

- **passport** (Array of strings) -> Works the same way the _documents_ configuration setting does. Used for capture of passports, TD3 format. This setting can not be used in conjunction with _documents_, _documentsMinimal_ and _documentsPaper_. **Example**:passport: [‘XX_Passport_YYYY’]

- **logEventsToConsole** (Boolean) -> Whether or not to log events (camera start, etc) to the console.

- **targetSelector** (String or Object) -> The (unique inside the HTML document) CSS selector of the element VDSelfie should attach to. Object created by HTMLElement method selection like getElementByID could be passed too.

- **manualCaptureEnableDelay** (Number) -> Duration in milliseconds after which VDDocument allows manual capture by touch or click event.

- **docLibHelperPath** (String) -> Set custom path to load docLibHelper library in VDDocument, always relative to index.html.

## Text Configuration

- **continueText** (String) -> Label for the button that allows the user to click and move to the next stage.

- **infoAlertSingleSidedDocument** (String) -> Text of "heads-up" dialog warning the user that detection of one-sided document is about to start.

- **infoAlertTwoSidedDocument** (String) -> Text of "heads-up" dialog warning the user that detection of two-sided document is about to start.

- **infoReviewBlurImageText** (String) -> Text to display to let the user being informed about blurriness in the captured image.

- **infoReviewImageText** (String) -> Text to display to let the user review the captured image.

- **manualCaptureText** (String) -> Text to let the user know that capture can be triggered manually, on desktops and laptop devices.

- **manualCaptureTextMobile** (String) -> Text to let the user know that capture can be triggered manually, on mobile devices.

- **obverseNotFoundText** (String) -> Text with instructions to user during detection of obverse of document.

- **reverseNotFoundText** (String) -> Text with instructions to user during detection of reverse of document.

- **passportNotFoundText** (String) -> Instructions to user during detection of passport.

- **passportMRZError** (String) -> Text to display when many characters of MRZ passport are not visible.

- **repeatText** (String) -> Label for the button that allows the user to repeat detection, if the captured image was not clear.

- **fontSize** (Number) -> Set a number that will represent the percent of the calculated font size, if the number is positive it will increase the font size that porcentage, but if it is negative it will decrease it.

## Style Configuration

- **borderColorCenteringAidDefault** (String) -> Border color of the centering aid, when detection is active but no document is being "seen" yet.

- **borderColorCenteringAidDetecting** (String) -> Border color of the centering aid, when a document is being "seen".

- **borderColorCenteringAidDetectingSuccess** (String) -> Border color of the centering aid, when a document has been successfully captured.

- **borderColorCenteringAidInactive** (String) -> Border color of the centering aid, before detection process starts, or when the sdk asks for the device to be used in portrait mode.

- **confirmationColorTick** (String) -> Tick color icon when capture success.

- **confirmationDialogBackgroundColor** (String) -> Background color of confirmation dialogs.

- **confirmationDialogButtonBackgroundColor** (String) -> Background color of buttons in confirmation dialogs.

- **confirmationCaptureButtonBackgroundColor** (String) -> Background color of button in confirmation capture dialog.

- **confirmationDialogButtonTextColor** (String) -> Text color of buttons in confirmation dialogs.

- **confirmationDialogLinkTextColor** (String) -> Text color of buttons that look like links in confirmation dialogs.

- **confirmationDialogTextColor** (String) -> Text color of confirmation dialogs.

- **detectionMessageBackgroundColor** (String) -> Background color of messages overlayed during the detection process.

- **detectionMessageTextColor** (String) -> Text color of messages overlayed during the detection process.

- **infoAlertShow** (Boolean) -> Whether to display a "heads-up" confirmation dialog warning detection is about to start.

- **infoModalShow** (Boolean) -> Whether to display a modal helper dialog to explain the user the needed actions to success within the authentication process.

- **isInfoModalShow** (Boolean) -> Display button that allow to Display a Info user Modal with steps instrucctions.

- **infoUserSliderButtonText** (String) -> Label for continue button in help user modal.

- **infoUserDocumentTitle** (String) -> Config title document help user modal.

- **infoUserImagesPath** (String) -> Config path with the icons for help user modal By default are used internal icons.

- **infoUserDocumentMedia** (String) -> Config icon fileName to display at document help user modal.

- **infoUserDocumentBackgroundColorTop** (String) -> Config background color top document help user modal.

- **infoUserDocumentBackgroundColor** (String) -> Config background color document help user modal.

- **infoUserDocumentBackgroundColorButton** (String) -> Config button background color document help user modal.

- **outerGlowCenteringAidDefault** (String) -> Color of the translucent area surrounding the border of the centering aid, when detection is active but no document is being detected yet.

- **outerGlowCenteringAidDetecting** (String) -> Color of the translucent area surrounding the border of the centering aid, when a document is being detected.

- **reviewImage** (Boolean) -> Whether to display a confirmation dialog displaying the captured image, for the user to have a choice between continuing to the next step or redoing capture.

- **sdkBackgroundColorInactive** (String) -> Background color of the sdk before detection starts.

- **firstArrow** (String) -> Color of the first arrow display when smart document is detected.

- **secondArrow** (String) -> Color of the second arrow display when smart document is detected.

- **thirdArrow** (String) -> Color of the third arrow display when smart document is detected.

- **fourthArrow** (String) -> Color of the fourth arrow display when smart document is detected.

- **fifthArrow** (String) -> Color of the fifth arrow display when smart document is detected.

## Updated Style Configuration

- **infoUserSlide6Title** UPDATED FOR => **infoUserDocumentTitle** (String) -> Config title document help user modal.

- **infoUserSlide6Media** UPDATED FOR => **infoUserDocumentMedia** (String) -> Config media document help user modal.

- **infoUserSlide6BackgroundColorTop** UPDATED FOR => **infoUserDocumentBackgroundColorTop** (String) -> Config background color top document help user modal.

- **infoUserSlide6BackgroundColor** UPDATED FOR => **infoUserDocumentBackgroundColor** (String) -> Config background color document help user modal.

## Error Configuration

- **displayErrors** (Boolean) -> Whether to see errors in the UI. These errors would display if browser requirements are not met or if the camera fails to start.

- **permissionRefused** (String) -> Warning to display to user if the browser is blocking the SDK from accessing the media device.

- **permissionRefusedTitle** (String) -> Title of warning to user if the browser is blocking the SDK from accessing the media device

- **videoErrorConstraint** (String) -> Error to display to user if media constraint is not correct.

- **videoErrorConstraintActionLabel** (String) -> Text label button to display if media constraint is not correct.

- **videoErrorDefault** (String) -> Error to display to user if there is a media default error.

- **videoErrorDefaultActionLabel** (String) -> Text label button to display if there is a media default error.

- **videoErrorNotFound** (String) -> Error to display to user if media device is not found.

- **videoErrorNotFoundActionLabel** (String) -> Text label button to display if media device is not found.

- **videoErrorPermission** (String) -> Error to display to user if media does not have permission.

- **videoErrorPermissionActionLabel** (String) -> Text label button to display if media does not have permission.

- **videoErrorUnavailable** (String) -> Error to display to user if media device is unavailable.

- **videoErrorUnavailableActionLabel** (String) -> Text label button to display if media device is unavailable.

- **webrtcUnsupportedText** (String) -> Text to display to the user if webRTC is not supported on the user’s browser. This message will only be visible if displayErrors is set to true.

## Customization Examples

<img src="img/Imagen01.png" width="45%"/>
<img src="img/Imagen02.png" width="45%"/>
<img src="img/Imagen04.png" width="45%"/>
<img src="img/Imagen05.png" width="45%"/>
<img src="img/Imagen06.png" width="45%"/>
<img src="img/Imagen07.png" width="45%"/>
<img src="img/Imagen08.png" width="45%"/>
<img src="img/Imagen03.png" width="45%"/>

## Default Values

The number inside () on each row corresponds with an example in Customization Examples section.

| **Parameter**                                     | **Value**                                                                                                                                                                                                                                                                                                                                 |
| :------------------------------------------------ | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| blurDetectorActive                                | false                                                                                                                                                                                                                                                                                                                                     |
| detectionTimeout                                  | 300000                                                                                                                                                                                                                                                                                                                                    |
| logEventsToConsole                                | false                                                                                                                                                                                                                                                                                                                                     |
| manualCaptureEnableDelay                          | 3000                                                                                                                                                                                                                                                                                                                                      |
| continueText (**13**)                             | CONTINUE <br> CONTINUAR                                                                                                                                                                                                                                                                                                                   |
| infoAlertSingleSidedDocument (**15**)             | A picture will be taken of the FRONT <br>Se tomará una foto del anverso                                                                                                                                                                                                                                                                   |
| infoAlertTwoSidedDocument (**15**)                | The FRONT and BACK of the document will be captured <br>Se fotografiará el anverso y reverso                                                                                                                                                                                                                                              |
| infoReviewBlurImageText (**11**)                  | It seems that the photo is blurred Please repeat the process <br>Parece que la foto está borrosa. Por favor, repita el proceso                                                                                                                                                                                                            |
| infoReviewImageText (**11**)                      | Check if the photo is readable and focused <br>Comprueba que la foto es legible y está bien enfocada                                                                                                                                                                                                                                      |
| manualCaptureText (**7**)                         | CLICK HERE to capture <br>Haga CLICK AQUÍ para capturar                                                                                                                                                                                                                                                                                   |
| manualCaptureTextMobile (**7**)                   | Touch the screen to capture <br>Toque la imagen para capturar                                                                                                                                                                                                                                                                             |
| obverseNotFoundText (**7**)                       | Fit the FRONT of the document <br>Encaje la parte DELANTERA                                                                                                                                                                                                                                                                               |
| reverseNotFoundText (**7**)                       | Fit the BACK of the document <br>Encaje la parte TRASERA                                                                                                                                                                                                                                                                                  |
| passportNotFoundText (**7**)                      | Fit the passport in the frame <br>Encaje la página del pasaporte                                                                                                                                                                                                                                                                          |
| passportMRZError (**7**)                          | Check that the two bottom lines are fully visible <br>Comprueba que las dos líneas de texto inferiores son totalmente visibles                                                                                                                                                                                                            |
| repeatText (**12**)                               | REPEAT <br>QUIERO REPETIR                                                                                                                                                                                                                                                                                                                 |
| fontSize                                          | 1                                                                                                                                                                                                                                                                                                                                         |
| docLibHelperPath                                  | /docLibHelper                                                                                                                                                                                                                                                                                                                             |
| borderColorCenteringAidDefault (**6**)            | #FFFFFF                                                                                                                                                                                                                                                                                                                                   |
| borderColorCenteringAidDetecting (**8**)          | #00FF00                                                                                                                                                                                                                                                                                                                                   |
| borderColorCenteringAidDetectingSuccess (**8**)   | #FFFFFF                                                                                                                                                                                                                                                                                                                                   |
| borderColorCenteringAidInactive (**14**)          | #717171                                                                                                                                                                                                                                                                                                                                   |
| confirmationColorTick (**9**)                     | #737373                                                                                                                                                                                                                                                                                                                                   |
| confirmationDialogBackgroundColor (**10**)        | #4D4B4E                                                                                                                                                                                                                                                                                                                                   |
| confirmationDialogButtonBackgroundColor           | #12676F                                                                                                                                                                                                                                                                                                                                   |
| confirmationCaptureButtonBackgroundColor (**13**) | #26BAB8                                                                                                                                                                                                                                                                                                                                   |
| confirmationDialogButtonTextColor (**13**)        | #FFFFFF                                                                                                                                                                                                                                                                                                                                   |
| confirmationDialogLinkTextColor (**12**)          | #26BAB8                                                                                                                                                                                                                                                                                                                                   |
| confirmationDialogTextColor (**11**)              | #FFFFFF                                                                                                                                                                                                                                                                                                                                   |
| detectionMessageBackgroundColor (**7**)           | #424242                                                                                                                                                                                                                                                                                                                                   |
| detectionMessageTextColor (**7**)                 | #FFFFFF                                                                                                                                                                                                                                                                                                                                   |
| infoAlertShow                                     | true                                                                                                                                                                                                                                                                                                                                      |
| infoUserImagesPath                                | ''                                                                                                                                                                                                                                                                                                                                        |
| infoModalShow                                     | true                                                                                                                                                                                                                                                                                                                                      |
| isInfoModalShow                                   | false                                                                                                                                                                                                                                                                                                                                     |
| infoUserSliderButtonText (**4**)                  | Start <br>Empezar                                                                                                                                                                                                                                                                                                                         |
| infoUserDocumentBackgroundColor (**5**)           | #E4F6F6                                                                                                                                                                                                                                                                                                                                   |
| infoUserDocumentBackgroundColorButton (**4**)     | #26BAB8                                                                                                                                                                                                                                                                                                                                   |
| infoUserDocumentBackgroundColorTop (**1**)        | #26BAB8                                                                                                                                                                                                                                                                                                                                   |
| infoUserDocumentMedia (**2**)                     | helpIconDocumentCenter                                                                                                                                                                                                                                                                                                                    |
| infoUserDocumentTitle (**3**)                     | 1. CENTER your document in the frame 2. Make sure your image DOES NOT show glare or shadow 3. The photo will be captured AUTOMATICALLY <br>1. CENTRE su documento en el marco 2. Asegúrese de que su documento NO muestra brillos o sombras. 3. La fotografía se capturará de forma AUTOMÁTICA                                            |
| outerGlowCenteringAidDefault                      | #000000                                                                                                                                                                                                                                                                                                                                   |
| outerGlowCenteringAidDetecting (**8**)            | #001C00                                                                                                                                                                                                                                                                                                                                   |
| reviewImage                                       | true                                                                                                                                                                                                                                                                                                                                      |
| sdkBackgroundColorInactive (**21**)               | #2E2E2E                                                                                                                                                                                                                                                                                                                                   |
| displayErrors                                     | false                                                                                                                                                                                                                                                                                                                                     |
| permissionRefused (**18**)                        | It is necessary to allow access to the camera to capture and verify the document and the user <br>Se necesita tener acceso a la cámara del dispositivo para la captura y verificación del documento y del usuario                                                                                                                         |
| permissionRefusedTitle                            | ''                                                                                                                                                                                                                                                                                                                                        |
| videoErrorConstraint (**18**)                     | Please check that the device has a camera with a minimum resolution of 640 x 480. <br>Por favor, compruebe que el dispositivo dispone de cámara de resolución mínima 640 x 480.                                                                                                                                                           |
| videoErrorConstraintActionLabel (**19**)          | Retry <br>Reintentar                                                                                                                                                                                                                                                                                                                      |
| videoErrorDefault (**18**)                        | Sorry, there was an error starting the camera <br>Lo sentimos, se ha producido un error iniciando la cámara                                                                                                                                                                                                                               |
| videoErrorDefaultActionLabel (**19**)             | Retry <br>Reintentar                                                                                                                                                                                                                                                                                                                      |
| videoErrorNotFound (**18**)                       | Please check that the device has a camera <br>Por favor, compruebe que el dispositivo dispone de cámara                                                                                                                                                                                                                                   |
| videoErrorNotFoundActionLabel (**19**)            | Retry <br>Reintentar                                                                                                                                                                                                                                                                                                                      |
| videoErrorPermission (**18**)                     | Please allow the page to access the camera <br>Por favor, permita a la página acceso a la cámara                                                                                                                                                                                                                                          |
| videoErrorPermissionActionLabel (**19**)          | Retry <br>Reintentar                                                                                                                                                                                                                                                                                                                      |
| videoErrorUnavailable (**18**)                    | Please check that the device camera is not being used by another program <br>Por favor, compruebe que la cámara del dispositivo no está siendo usada por otro programa                                                                                                                                                                    |
| videoErrorUnavailableActionLabel (**19**)         | Retry <br>Reintentar                                                                                                                                                                                                                                                                                                                      |
| firstArrow (**20**)                               | #e6f4ff                                                                                                                                                                                                                                                                                                                                   |
| secondArrow (**20**)                              | #b3deff                                                                                                                                                                                                                                                                                                                                   |
| thirdArrow (**20**)                               | #80c8ff                                                                                                                                                                                                                                                                                                                                   |
| fourthArrow (**20**)                              | #33a7ff                                                                                                                                                                                                                                                                                                                                   |
| fifthArrow (**20**)                               | #0082e6                                                                                                                                                                                                                                                                                                                                   |
| webrtcUnsupportedText (**18**)                    | Not supported browser. You must repeat the process: If you are on an iPhone you must use the Safari browser. In all other cases you can use Chrome, Firefox or Opera <br>Navegador no compatible. Debe repetir el proceso: si está en un iPhone debe emplear el navegador Safari. En el resto de casos puede usar Chrome, Firefox u Opera |
