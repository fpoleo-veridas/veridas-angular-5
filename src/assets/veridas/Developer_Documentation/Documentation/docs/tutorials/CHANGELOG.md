# CHANGELOG

## Version 3.0.2 - 23/03/2021

- Fixed error on infoAlertShow.
- Fixed a style error on Detection arrows at reverse detection stage.

## Version 3.0.1 - 24/02/2021

- Updated documentation: see integration section.
- Fix: Desktop mode browsers do not change behaviour.
- Fix: IPads with IOS 13 or higher versions it does not launch the camera in crystal mode and it does not behaviour in desktop mode.

## Version 3.0.0 - 29/01/2021

- New functionality for bidi codes read.
- New info User modal that replace old Sliders. **THE CONFIG HAS CHANGED!** (See at Customization section on Updated Style Configuration).
- New customization of the button at the info user modal.
- Smart documents functionality support for Spain and Mexico Identification Documents. This new functionallity will show arrows to show the position of the detected documents aswell as the corresponding template. The list of the documents is on **AUTOMATICALLY CLASSIFIED DOCUMENTS** inside DOCUMENTS page.
- Color customization of smart detection arrows (See at Customization section).
- New events: obverseDetection_type, reverseDetection_type.

## Version 2.6.2 - 22/03/2021

- Fixed error on infoAlertShow.

## Version 2.6.1 - 29/10/2020

- New events: repeatClicked, continueClicked.

## Version 2.6.0 - 20/10/2020

- New feature: Passport detection improved.
- New feature: Enabled blurred image detection.
- New feature: Image capture improvement.
- New feature: Global method to get the current sdk version.
- New feature: Font size of all sdk text configurable.
- New support: Edge Browser.
- New support: English version of the SDK, now if the browser language is configured in a non Spanish language it will display all sdk text in English.
- News documents supported.
- New documentation: HTML doc based on JSDocs.
- Deleted wasm file dependency.
- Reduced the size of external dependencies the 3.5M to 2.8M.
- Fix: fixed tutorial styles bugs.
- Fix: fixed capture in IR cameras.

## Version 2.5.1 - 07/08/2020

- Increased security through detecting an external source that injects an image.
- Fixed bug when integrating SDK into specific tags.
