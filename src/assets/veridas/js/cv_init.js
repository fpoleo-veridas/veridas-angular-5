try {
  var Module = {
    onRuntimeInitialized: function () {
      if (window.cv instanceof Promise) {
        window.cv.then(target => {
          window.cv = target;
          sdkStart();
        });
      } else {
        sdkStart();
      }
    },
  };
} catch (e) {
  console.error(e);
}

function sdkStart() {
  const VDDocument = makeVDDocumentWidget();
  VDDocument({
    targetSelector: '#target',
    documents: [
      'ES2',
    ],
    docLibHelperPath: '/docLibHelper'
  });
}
function CVLoadError() {
  Module.printErr('Failed to load/initialize cv.js');
}
